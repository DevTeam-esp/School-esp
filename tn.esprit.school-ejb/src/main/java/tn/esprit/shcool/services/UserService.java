package tn.esprit.shcool.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import tn.esprit.school.entities.User;
import tn.esprit.school.utilities.EncrypterClass;
import tn.esprit.school.utilities.GenericService;



@LocalBean
@Stateless
public class UserService extends GenericService<User>{

	private final static String UNIT_NAME = "tn.esprit.school-ejb";
	@PersistenceContext(unitName = UNIT_NAME)
	private EntityManager em;
	
	
	public UserService() {
		super(User.class);
	}
	public User findUserByUsername(String username){
		User result=null;
		
		 result = (User)em
				 .createQuery("select u from User u where u.userName = :userName")
				 .setParameter("userName", username)
				 .getSingleResult();
	return result;
}

	
	public User CheckAuth(String username,String password){
		
		User result=null;
		result=this.findUserByUsername(username);
		try {
			return EncrypterClass.Password.match(password,result.getPassword())? result : null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public User findUserByEmail(String email){
		
		
		TypedQuery<User> query = em.createQuery(

				"select u from User u "+
				"where u.email=:email"			
				,User.class
				
				);
				
			query.setParameter("email",email);
		
				
			User user = null ;
		    
		    
		    try{
		    user = query.getSingleResult();	
		    }catch(NoResultException e){
		    	
		    	System.out.println("user not found");
		    	
		    }
		    return user;

		
		
		
	}
	
	
	

}
