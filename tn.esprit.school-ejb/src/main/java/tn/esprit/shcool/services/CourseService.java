package tn.esprit.shcool.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.school.entities.Course;
import tn.esprit.school.utilities.GenericService;


@LocalBean
@Stateless
public class CourseService extends GenericService<Course>{

	public CourseService() {
		super(Course.class);
		
	}
}
