package tn.esprit.school.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import tn.esprit.school.enums.CourseType;




/**
 * Entity implementation class for Entity: Course
 *
 */
@Entity

public class Course implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	
	private String title;
	
	@Enumerated(EnumType.STRING)
	private CourseType courseType;
	
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	
	private String description;
	
	@Column(length = 100000)
	private String Content;
	
	
	private int timeRequired;
	
	@ManyToOne
	private User user;
	
	public Course() {
		super();
	}
   
	
	
}
