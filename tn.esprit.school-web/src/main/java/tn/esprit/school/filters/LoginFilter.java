package tn.esprit.school.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.esprit.MangedBean.LoginBean;



public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest Request, ServletResponse Response, FilterChain chain)
			throws IOException, ServletException {
		    LoginBean loginBean = (LoginBean)((HttpServletRequest)Request).getSession().getAttribute("loginBean");
	        String contextPath = ((HttpServletRequest)Request).getContextPath();
	        if (loginBean == null || !loginBean.isLoggedIn()) {
	            
	            ((HttpServletResponse)Response).sendRedirect(contextPath + "/login.xhtml");
	        }
	         
		
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
