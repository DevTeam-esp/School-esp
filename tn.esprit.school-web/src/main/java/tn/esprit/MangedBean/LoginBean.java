package tn.esprit.MangedBean;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.school.entities.User;
import tn.esprit.shcool.services.UserService;





@ManagedBean
@SessionScoped
public class LoginBean {
   private boolean loggedIn;
   
   @EJB
   UserService us;

    
   private String username;
   private String password;
   private tn.esprit.school.entities.User currentUser;
   

   @ManagedProperty(value="#{navigationBean}")
   private NavigationBean navigationBean;
   
   
   
   /**
    * Login operation.
    * @return
    */
   public String doLogin() {
   	
   	
	currentUser=us.CheckAuth(username, password);
   	System.out.println(currentUser);
   	if(currentUser!=null){
           loggedIn = true;
           return navigationBean.redirectToWelcome();
   	}

        
       // Set login ERROR
       FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
       msg.setSeverity(FacesMessage.SEVERITY_ERROR);
       FacesContext.getCurrentInstance().addMessage(null, msg);
        
       // To to login page
       return navigationBean.toLogin();
        
   }
    
   public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	/**
    * Logout operation.
    * @return
    */
   public String doLogout() {
       // Set the paremeter indicating that user is logged in to false
       loggedIn = false;
        
       // Set logout message
       FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
       msg.setSeverity(FacesMessage.SEVERITY_INFO);
       FacesContext.getCurrentInstance().addMessage(null, msg);
        
       return navigationBean.toLogin();
   }

   // ------------------------------
   // Getters & Setters 
    
   public String getUsername() {
       return username;
   }

   public void setUsername(String username) {
       this.username = username;
   }

   public String getPassword() {
       return password;
   }

   public void setPassword(String password) {
       this.password = password;
   }

   public boolean isLoggedIn() {
       return loggedIn;
   }

   public void setLoggedIn(boolean loggedIn) {
       this.loggedIn = loggedIn;
   }

   public void setNavigationBean(NavigationBean navigationBean) {
       this.navigationBean = navigationBean;
   }

}
