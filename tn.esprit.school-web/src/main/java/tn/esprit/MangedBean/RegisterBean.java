package tn.esprit.MangedBean;

import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

<<<<<<< HEAD:tn.esprit.school-web/src/main/java/tn/esprit/MangedBean/RegisterBean.java
import tn.esprit.MangedBean.navigationBean;
=======
import tn.esprit.MangedBean.NavigationBean;
>>>>>>> e5c66c4aaa99beff8f6ae7e764b4348371d70a55:tn.esprit.school-web/src/main/java/tn/esprit/school/mbeans/RegisterBean.java
import tn.esprit.school.entities.User;
import tn.esprit.school.enums.UserType;
import tn.esprit.shcool.services.UserService;



@ManagedBean(name="registerBean")
@RequestScoped
public class RegisterBean {

	
	
	private String email;
	private String firstName;
	private String lastName;
	private Date birthDay;
	private String password;
	@ManagedProperty(value="#{navigationBean}")
	private navigationBean bean;
	
	@EJB
	private UserService userService;
	
	public String doRegister(){
		
		String NavigateTo = "";
		User user = userService.findUserByEmail(email);
		
		if(user == null){
			
			User u = new User();
			u.setActive(false);
			u.setBirthDay(birthDay);
			u.setEmail(email);
			u.setFirstName(firstName);
			u.setLastName(lastName);
			u.setPassword(password);
			u.setType(UserType.ETUDIANT);
			userService.create(u);
			NavigateTo = bean.redirectToWelcome();
		}else{
			
			FacesContext.getCurrentInstance()
			.addMessage("form:btnRegister",new FacesMessage("Email exist already"));
		}
		return NavigateTo;
	}
	
	
	public RegisterBean() {
		
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Date getBirthDay() {
		return birthDay;
	}


	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public navigationBean getBean() {
		return bean;
	}


	public void setBean(navigationBean bean) {
		this.bean = bean;
	}


	public UserService getUserService() {
		return userService;
	}


	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	

}
